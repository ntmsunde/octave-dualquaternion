## Copyright (C) 2018   Nicholas Sunderland
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{ret} =} subsref(@var{dq}, @var{s})
## Subscripted reference for dual quaternions.
## @end deftypefn

## Author: Nicholas Sunderland <ntmsunde@uwaterloo.ca>
## Created: March 2018
## Version: 0.1

function ret = subsref(dq, s)
	if (numel(s) == 0)
		ret = dq;
		return;
	endif

	switch (s(1).type)
		case "."
			switch (tolower(s(1).subs))
				case "a"
					ret = subsref(dq.a, s(2:end));
				case "b"
					ret = subsref(dq.b, s(2:end));
				otherwise
					error("dualquaternion: invalid subscript")
			endswitch
		case "()"
			a = subsref(dq.a, s(1));
			b = subsref(dq.b, s(1));
			tmp = dualquaternion(a, b);
			ret = subsref(tmp, s(2:end));

		otherwise
			error("dualquaternion: invalid subscript type '%s'", s(1).type);
	endswitch
endfunction
