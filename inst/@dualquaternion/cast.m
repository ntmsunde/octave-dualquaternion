## Copyright (C) 2018   Nicholas Sunderland
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{dq} =} cast(@var{dq}, @var{type})
## Convert the components of dual quaternion @var{dq} to data type @var{type}.
## Valid types are those supported by quaternion (int8, uint8, int16, uint16,
## int32, uint32, int64, uint64, double, single and logical as of v0.1).
## @end deftypefn

## Author: Nicholas Sunderland <ntmsunde@uwaterloo.ca>
## Created: March 2018
## Version: 0.1

function dq = cast(dq, type)
	if (nargin != 2)
		print_usage();
	endif

	dq.a = cast(dq.a, type);
	dq.b = cast(dq.b, type);
endfunction
