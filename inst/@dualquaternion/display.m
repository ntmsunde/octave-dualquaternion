## Copyright (C) 2018   Nicholas Sunderland
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {} display(@var{dq})
## Display the dualquaternion @var{dq}. Used by Octave internally.
## @end deftypefn

## Author: Nicholas Sunderland <ntmsunde@uwaterloo.ca>
## Created: March 2018
## Version: 0.1

function display(dq)
	name = inputname(1);
	a = __quat2str__(dq.a);
	b = __quat2str__(dq.b);
	disp([name, " = ", a, " + ", "ε", b]);
endfunction

function str = __quat2str__(q) 
	str = [ ...
		"(", ...
		num2str(q.w, 4), ...
		__sign__(q.x), num2str(abs(q.x), 4), "i", ...
		__sign__(q.y), num2str(abs(q.y), 4), "j", ...
		__sign__(q.z), num2str(abs(q.z), 4), "k", ...
		")" ...
	];
endfunction

function str = __sign__(num)
	if (num < 0)
		str = " - ";
	else
		str = " + ";
	endif
endfunction
