## Copyright (C) 2018   Nicholas Sunderland
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{dq} =} dualquaternion(@var{d0})
## @deftypefnx {Function File} {@var{dq} =} dualquaternion(@var{q1}, @var{q2})
## @deftypefnx {Function File} {@var{dq} =} dualquaternion(@var{a}, @var{b}, @var{c}, @var{d}, @var{e}, @var{f}, @var{g}, @var{h})
##
## Construct a new dual quaternion.
## Can construct using copy constructor, single quaternion (sets b to the zero quaternion),
## single real value (constructs a dual quaternion with a = 1 and b = 0), two elements (which will each be
## constructed into quaternions), or the eight coefficients.
##
## @end deftypefn

## Author: Nicholas Sunderland <ntmsunde@uwaterloo.ca>
## Created: March 2018
## Version: 0.1

function dq = dualquaternion(a, b, c, d, e, f, g, h)
	switch (nargin)
		case 1
			if (isa(a, "dualquaternion"))
				dq = a;
				return;
			elseif (isa(a, "quaternion"))
				b = quaternion(0,0,0,0);
			elseif (is_real_array(a)) # not quite right...
				a = quaternion(a,0,0,0);
				b = quaternion(0,0,0,0);
			else
				print_usage();
			endif
		case 2
			a = quaternion(a);
			b = quaternion(b);
		case 8
			a = quaternion(a, b, c, d);
			b = quaternion(e, f, g, h);
		otherwise
			print_usage();
	endswitch

	dq = class(struct("a", a, "b", b), "dualquaternion");
endfunction
