## Copyright (C) 2018   Nicholas Sunderland
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{dq} =} times(@var{dq1}, @var{dq2})
## Element-wise multiplication of @var{dq1} and @var{dq2}. Used by Octave for "dq1 .* dq2".
## @end deftypefn

## Author: Nicholas Sunderland <ntmsunde@uwaterloo.ca>
## Created: March 2018
## Version: 0.1

function dq = times(dq1, dq2)
	if (nargin != 2)
		print_usage();
	endif

	if (is_real_array(dq1))
		dq = dualquaternion(dq1 .* dq2.a, dq1 .* dq2.b);
		return;
	elseif (is_real_array(dq2))
		dq = dualquaternion(dq2 .* dq1.a, dq2 .* dq1.b);
		return;
	elseif (isa(dq1, "quaternion"))
		dq1 = dualquaternion(dq1);
	elseif (isa(dq2, "quaternion"))
		dq2 = dualquaternion(dq2);
	elseif (!isa(dq1, "dualquaternion") || !isa(dq2, "dualquaternion"))
		print_usage();
		return;
	endif

	dq = dualquaternion(dq1.a .* dq2.a, dq1.a .* dq2.b + dq1.b .* dq2.a);
endfunction
