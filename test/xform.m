disp("Note: some results may be off due to floating point error");

function xformtest(theta, raxis, t, p)
	if (norm(raxis) == 0)
		error("Cannot rotate around zero vector.");
	endif

	disp(["Rotation angle: ", num2str(theta)]);
	disp(["Axis of rotation: ", num2str(raxis)]);
	disp(["Translation vector: ", num2str(t)]);
	disp(["Point to transform: ", num2str(p)]); 

	% quaternion for rotation 
	r = raxis / norm(raxis);
	r = sin(theta / 2) * r;
	rq = quaternion(cos(theta / 2), r(1), r(2), r(3));
	
	% quaternion for translation
	tq = quaternion(0, t(1), t(2), t(3));

	% dualquaternion for full transformation
	trans = dualquaternion(rq, (tq * rq) / 2);

	% point as dualquaternion
	pd = dualquaternion(1, quaternion(0, p(1), p(2), p(3)));

	disp("Transformation dualquaternion:");
	display(trans);

	disp("Transformation dualquaternion norm:");
	display(norm2(trans));

	% transformed point
	pdtrans = trans * pd * dualconj(conj(trans));
	ptrans = [pdtrans.b.i, pdtrans.b.j, pdtrans.b.k];

	disp(["Transformed point: ", num2str(ptrans)]);
endfunction

disp("---TEST 1---");
xformtest(2 * pi / 3, [1, 1, 1], [0, 0, 3], [5, 0, 0]);
disp(["Expected: ", num2str([0, 5, 3])]);

disp("---TEST 2---");
xformtest(0, [1, 0, 0], [1, 2, -3], [7, 0, 6]);
disp(["Expected: ", num2str([8, 2, 3])]);

disp("---TEST 3---");
xformtest(pi / 4, [3, 9, 1], [0, 0, 0], [1, 2, 2]);
disp(["Expected: ", num2str([2.1152 1.7098 1.2659])]);

disp("---TEST 4---");
xformtest(-0.3, [-5, 0, 7], [2, -1, 4], [5, 6, 3]);
disp(["Expected: ", num2str([8.2316 3.0143 7.8797])]);

disp("---TEST 5---");
xformtest(pi, [1, 1, 1], [1, 2, 3], [0, 0, 0]);
disp(["Expected: ", num2str([1 2 3])]);
